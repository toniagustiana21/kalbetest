create database KalbeTest

create table TblProduk(
Id int primary key identity(1,1),
ProductCode varchar(50) not null,
ProductName varchar(50) not null,
Quantity int not null,
Price decimal(18,0) not null,
dtInserted datetime not null
)

create table TblCustomer(
Id int primary key identity(1,1),
CustomerName varchar(50) not null,
CustomerAddress varchar(50) not null,
Gender bit not null,
TanggalLahir datetime not null,
dtInserted datetime not null
)

create table TblPenjualan(
Id int primary key identity(1,1),
CustomerId int not null,
ProductId int not null,
DateOrder datetime not null,
Quantity int not null
)

drop table TblProduk
drop table TblCustomer
drop table TblPenjualan

select * from TblProduk
select * from TblCustomer
select * from TblPenjualan