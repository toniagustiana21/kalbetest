﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using KalbeTest.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.web.Controllers
{
    public class ProdukController : Controller
    {
        private ProdukServices produkServices;

        public ProdukController(ProdukServices _produkServices)
        {
            this.produkServices = _produkServices;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblProduk> data = await produkServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.ProductName.ToLower().Contains(searchString.ToLower())
                || a.ProductCode.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.ProductName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.ProductName).ToList();
                    break;
            }

            return View(PaginatedList<TblProduk>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            TblProduk data = new TblProduk();
            return PartialView(data);

        }
        [HttpPost]
        public async Task<IActionResult> Create(TblProduk dataParam)
        {
            VMResponse respon = await produkServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblProduk data = await produkServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblProduk dataParam)
        {
            VMResponse respon = await produkServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblProduk data = await produkServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await produkServices.Delete(id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblProduk data = await produkServices.GetDataById(id);
            return PartialView(data);
        }
    }
}
