﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using KalbeTest.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.web.Controllers
{
    public class CustomerController : Controller
    {
        private CustumerServices custumerServices;

        public CustomerController(CustumerServices _custumerServices)
        {
            this.custumerServices = _custumerServices;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblCustomer> data = await custumerServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.CustomerName.ToLower().Contains(searchString.ToLower())
                || a.CustomerAddress.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.CustomerName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.CustomerName).ToList();
                    break;
            }
            return View(PaginatedList<TblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            TblCustomer data = new TblCustomer();
            return PartialView(data);

        }
        [HttpPost]
        public async Task<IActionResult> Create(TblCustomer dataParam)
        {
            VMResponse respon = await custumerServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblCustomer data = await custumerServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblCustomer dataParam)
        {
            VMResponse respon = await custumerServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblCustomer data = await custumerServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await custumerServices.Delete(id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblCustomer data = await custumerServices.GetDataById(id);
            return PartialView(data);
        }
    }
}
