﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using KalbeTest.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.web.Controllers
{
    public class PenjualanController : Controller
    {
        private PenjualanServices penjualanServices;
        private CustumerServices custumerServices;
        private ProdukServices produkServices;

        public PenjualanController(PenjualanServices _penjualanServices, CustumerServices _custumerServices, ProdukServices _produkServices)
        {
            this.penjualanServices = _penjualanServices;
            this.custumerServices = _custumerServices;
            this.produkServices = _produkServices;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                              string searchString,
                                              string currentFilter,
                                              int? pageNumber,
                                              int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;

            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblPenjualan> data = await penjualanServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.ProductName.ToLower().Contains(searchString.ToLower())
                || a.CustomerName.ToLower().Contains(searchString.ToLower())
              

                ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.CustomerName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.CustomerName).ToList();
                    break;
            }

            return View(PaginatedList<VMTblPenjualan>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMTblPenjualan data = new VMTblPenjualan();

            List<TblProduk> listProduk = await produkServices.GetAllData();
            ViewBag.ListProduk = listProduk;

            List<TblCustomer> listCustomer= await custumerServices.GetAllData();
            ViewBag.ListCustomer = listCustomer;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblPenjualan dataParam)
        {

            VMResponse respon = await penjualanServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });

            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblPenjualan data = await penjualanServices.GetDataById(id);

            List<TblProduk> listProduk = await produkServices.GetAllData();
            ViewBag.ListProduk = listProduk;

            List<TblCustomer> listCustomer = await custumerServices.GetAllData();
            ViewBag.ListCustomer = listCustomer;

            return PartialView(data);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblPenjualan dataParam)
        {
            VMResponse respon = await penjualanServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMTblPenjualan data = await penjualanServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblPenjualan data = await penjualanServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int Id)
        {

            VMResponse respon = await penjualanServices.Delete(Id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", Id);
        }


    }
}
