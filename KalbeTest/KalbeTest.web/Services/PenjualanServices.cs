﻿using KalbeTest.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace KalbeTest.web.Services
{
    public class PenjualanServices
    {
        public static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public PenjualanServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblPenjualan>> GetAllData()
        {
            List<VMTblPenjualan> data = new List<VMTblPenjualan>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiPenjualan/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblPenjualan>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(VMTblPenjualan dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiPenjualan/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMTblPenjualan> GetDataById(int id)
        {
            VMTblPenjualan data = new VMTblPenjualan();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiPenjualan/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblPenjualan>(apiResponse)!;

            return data;
        }

        

        public async Task<VMResponse> Edit(VMTblPenjualan dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiPenjualan/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiPenjualan/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
}
