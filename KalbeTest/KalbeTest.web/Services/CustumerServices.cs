﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace KalbeTest.web.Services
{
    public class CustumerServices
    {
        public static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CustumerServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblCustomer>> GetAllData()
        {
            List<TblCustomer> data = new List<TblCustomer>();

            string apiRespon = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblCustomer>>(apiRespon)!;

            return data;
        }

        public async Task<VMResponse> Create(TblCustomer dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<TblCustomer> GetDataById(int id)
        {
            TblCustomer data = new TblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblCustomer>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblCustomer dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync
                (RouteAPI + $"apiCustomer/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
