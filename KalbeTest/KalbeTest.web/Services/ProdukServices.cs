﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace KalbeTest.web.Services
{
    public class ProdukServices
    {
        public static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ProdukServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblProduk>> GetAllData()
        {
            List<TblProduk> data = new List<TblProduk>();

            string apiRespon = await client.GetStringAsync(RouteAPI + "apiProduk/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblProduk>>(apiRespon)!;

            return data;
        }

        public async Task<VMResponse> Create(TblProduk dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiProduk/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<TblProduk> GetDataById(int id)
        {
            TblProduk data = new TblProduk();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduk/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblProduk>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblProduk dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiProduk/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync
                (RouteAPI + $"apiProduk/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca data dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;


            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
