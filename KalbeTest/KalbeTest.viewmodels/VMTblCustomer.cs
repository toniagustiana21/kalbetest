﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KalbeTest.viewmodels
{
    public class VMTblCustomer
    {
        public int Id { get; set; }
        public string CustomerName { get; set; } = null!;
        public string CustomerAddress { get; set; } = null!;
        public bool Gender { get; set; }
        public DateTime TanggalLahir { get; set; }
        public DateTime DtInserted { get; set; }
    }
}
