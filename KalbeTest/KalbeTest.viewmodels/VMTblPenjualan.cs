﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KalbeTest.viewmodels
{
    public class VMTblPenjualan
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string? CustomerName { get; set; } 
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }
    }
}
