﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly KalbeTestContext db;
        private VMResponse respon = new VMResponse();

        public apiCustomerController(KalbeTestContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblCustomer> GetAllData()
        {
            List<TblCustomer> data = db.TblCustomers.Where(a => a.Id != 0).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblCustomer GetDataById(int id)
        {
            TblCustomer result = new TblCustomer();
            result = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }
        [HttpGet("CheckCustomerByName/{name}")]
        public bool CheckName(string name)
        {
            TblCustomer data = db.TblCustomers.Where(a => a.CustomerName == name).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.DtInserted = DateTime.Now;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
            }
            return respon;
        }


        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.CustomerName = data.CustomerName;
                dt.CustomerAddress = data.CustomerAddress;
                dt.Gender = data.Gender;
                dt.TanggalLahir = data.TanggalLahir;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.Message = "success";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "update failed :" + e.Message;
                }
            }
            else
            {
                respon.Message = "data not found";

            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                try
                {
                    db.Remove(dt);
                    db.SaveChanges();

                    respon.Message = $"Data {dt.CustomerName} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            return respon;
        }

    }
}
