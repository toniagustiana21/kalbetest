﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KalbeTest.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiPenjualanController : ControllerBase
    {
        private readonly KalbeTestContext db;
        private VMResponse respon = new VMResponse();

        public apiPenjualanController(KalbeTestContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblPenjualan> GetAllData()
        {
            List<VMTblPenjualan> data = (from p in db.TblPenjualans
                                         join c in db.TblCustomers on p.CustomerId equals c.Id
                                         join pro in db.TblProduks on p.ProductId equals pro.Id
                                         select new VMTblPenjualan
                                         {
                                             Id = p.Id,
                                             DateOrder = p.DateOrder,
                                             Quantity = p.Quantity,

                                             CustomerId = p.CustomerId,
                                             CustomerName = c.CustomerName,

                                             ProductId = p.ProductId,
                                             ProductName = pro.ProductName
                                         }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblPenjualan GetDataById(int id)
        {
            VMTblPenjualan data = (from p in db.TblPenjualans
                                   join c in db.TblCustomers on p.CustomerId equals c.Id
                                   join pro in db.TblProduks on p.ProductId equals pro.Id
                                   where p.Id == id
                                   select new VMTblPenjualan
                                   {
                                       Id = p.Id,
                                       DateOrder = p.DateOrder,
                                       Quantity = p.Quantity,

                                       CustomerId = p.CustomerId,
                                       CustomerName = c.CustomerName,

                                       ProductId = p.ProductId,
                                       ProductName = pro.ProductName
                                   }).FirstOrDefault()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblPenjualan data)
        {

            try
            {
                db.Add(data);

                TblProduk produk = db.TblProduks.Where(a => a.Id == data.ProductId).FirstOrDefault()!;
                produk.Quantity = produk.Quantity - data.Quantity;
                db.Update(produk);

                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblPenjualan data)
        {
            TblPenjualan dt = db.TblPenjualans.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                int qty_old = dt.Quantity;
                dt.DateOrder = data.DateOrder;
                dt.Quantity = data.Quantity;
                dt.CustomerId = data.CustomerId;
                dt.ProductId = data.ProductId;
                try
                {
                    db.Update(dt);

                    TblProduk produk = db.TblProduks.Where(a => a.Id == dt.ProductId).FirstOrDefault()!;

                    if(data.Quantity > qty_old)
                    {
                        produk.Quantity = produk.Quantity - (data.Quantity - qty_old);
                    }
                    else if(data.Quantity < qty_old)
                    {
                        produk.Quantity = produk.Quantity + (data.Quantity - qty_old);
                    }
                    db.Update(produk);
                    db.SaveChanges();
                    respon.Message = "success";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "update failed :" + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "data not found";

            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblPenjualan dt = db.TblPenjualans.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                try
                {
                    db.Remove(dt);

                    TblProduk produk = db.TblProduks.Where(a => a.Id == dt.ProductId).FirstOrDefault()!;
                    produk.Quantity = produk.Quantity + dt.Quantity;
                    db.Update(produk);

                    db.SaveChanges();

                    respon.Message = $"Data success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            return respon;
        }
    }
}
