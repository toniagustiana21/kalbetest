﻿using KalbeTest.datamodels;
using KalbeTest.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace KalbeTest.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProdukController : ControllerBase
    {
        private readonly KalbeTestContext db;
        private VMResponse respon = new VMResponse();

        public apiProdukController(KalbeTestContext _db)
        {
            this.db = _db;
        }
        [HttpGet("GetAllData")]
        public List<TblProduk> GetAllData()
        {
            List<TblProduk> data = db.TblProduks.Where(a => a.Id != 0).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblProduk GetDataById(int id)
        {
            TblProduk result = new TblProduk();
            result = db.TblProduks.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }
        [HttpGet("CheckProdukByName/{name}")]
        public bool CheckName(string name)
        {
            TblProduk data = db.TblProduks.Where(a => a.ProductName == name).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblProduk data)
        {
            data.DtInserted = DateTime.Now;
            data.ProductCode = GenerateCode();

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
            }
            return respon;
        }

        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = "P";
            string digit = "";

            TblProduk data = db.TblProduks.OrderByDescending(a => a.ProductCode).FirstOrDefault()!;

            if(data != null)
            {
                string codeLast = data.ProductCode;
                string[] codeSplit = codeLast.Split("P");
                int intLast = int.Parse(codeSplit[1]) + 1;
                digit = intLast.ToString("0000");
            }
            else
            {
                digit = "0001";
            }
            return code + digit;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblProduk data)
        {
            TblProduk dt = db.TblProduks.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.ProductCode = data.ProductCode;
                dt.ProductName = data.ProductName;
                dt.Quantity = data.Quantity;
                dt.Price = data.Price;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.Message = "success";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "update failed :" + e.Message;
                }
            }
            else
            {
                respon.Message = "data not found";

            }
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblProduk dt = db.TblProduks.Where(a => a.Id == id).FirstOrDefault()!;

            if(dt != null)
            {
                try
                {
                    db.Remove(dt);
                    db.SaveChanges();

                    respon.Message = $"Data {dt.ProductName} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + e.Message;
                }
            }
            return respon;
        }
    }
}
