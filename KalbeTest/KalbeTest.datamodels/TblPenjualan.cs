﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KalbeTest.datamodels
{
    [Table("TblPenjualan")]
    public partial class TblPenjualan
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }
    }
}
