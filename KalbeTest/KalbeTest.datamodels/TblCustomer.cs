﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KalbeTest.datamodels
{
    [Table("TblCustomer")]
    public partial class TblCustomer
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string CustomerName { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string CustomerAddress { get; set; } = null!;
        public bool Gender { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TanggalLahir { get; set; }
        [Column("dtInserted", TypeName = "datetime")]
        public DateTime DtInserted { get; set; }
    }
}
