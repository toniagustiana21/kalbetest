﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KalbeTest.datamodels
{
    [Table("TblProduk")]
    public partial class TblProduk
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string ProductCode { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string ProductName { get; set; } = null!;
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal Price { get; set; }
        [Column("dtInserted", TypeName = "datetime")]
        public DateTime DtInserted { get; set; }
    }
}
